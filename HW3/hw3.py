from Tkinter import *
from DataVis import *
from pendulum import *

__version__ = "1.0.0"

# N-DOF pendulum setup
pendy = Pendulum()
pendy.setNumberLinks(2)
pendy.setAngleICs([pi/2,pi/2])
pendy.simulate()

# GUI objects
root_GUI = Tk()
GUI_app = DataVis(pendy, master=root_GUI)
GUI_app.mainloop()
root_GUI.destroy()