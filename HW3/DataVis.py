# Data Visualization class (using TKinter) for hw3

from Tkinter import *
from pendulum import *
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.animation as anim
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

__version__ = "0.8.0"

class DataVis(Frame):

    def __init__(self, p, master=None):
        Frame.__init__(self,master)
        self.pend = Pendulum()
        self.pend.copy_values(p)
        self.pack()
        self.generateGUI(master)

    def generateGUI(self,master_root):      # function for calling indiv. object functions
        self.add_control_panel(self).pack({"side":"right"},padx=25,pady=25)
        self.add_var_plots(self).pack({"side":"left"},expand=0,fill='y',ipadx=100,ipady=40)
        self.add_anim_plot(self).pack({"side":"left"},expand=0,fill='none',padx=20,pady=20)

    def add_control_panel(self, parent,initial_run=1):      # contains sliders, buttons, plot controls
        self.control_panel=Frame(master=self)
        self.add_slider_panel(self.control_panel).pack({"side":"left"})
        self.add_var_choices(self.control_panel,initial_run).pack({"side":"right"})
        self.createQuitB(self.control_panel).pack({"side":"bottom"})
        self.add_resim_button(self.control_panel).pack({"side":"top"})
        return self.control_panel

    def add_slider_panel(self, parent):     # contains controls for simulation parameters
        self.slider_panel=Frame(master=self)
        self.add_DOF_slider(self.control_panel).pack()
        self.add_G_entrybox(self.control_panel).pack()
        # Add appropriate number of length, mass sliders
        self.length_array=[]
        self.mass_array=[]
        for i in range(0,self.pend.getNumberLinks()):
            L_temp = "{}".format(i+1)
            self.add_L_slider(self.control_panel,L_temp).pack()
        # split up to keep lengths, masses together in GUI
        for i in range(0,self.pend.getNumberLinks()):
            M_temp = "{}".format(i+1)
            self.add_M_slider(self.control_panel,M_temp).pack()
        return self.slider_panel

    def createQuitB(self,parent):   # normal program exit
        QUIT = Button(parent)
        QUIT["text"] = "QUIT"
        QUIT["fg"] = "red"
        QUIT["bg"] = "white"
        QUIT["command"] = self.quit
        return QUIT

    def add_resim_button(self,parent):  # to re-run simulation, refresh GUI
        resim = Button(parent)
        resim["text"] = "Re-Simulate",
        resim["command"] = self.re_sim
        return resim

    def re_sim(self):
        # change next slider values to previous slider values
        prev_DOF=self.pend.getNumberLinks()
        self.pend.setNumberLinks(self.DOF_slider.get())
        if (self.G_entrybox.get()):
            self.pend.setGravity(float(self.G_entrybox.get()))
        length_list=np.ones(self.pend.getNumberLinks())
        mass_list=np.ones(self.pend.getNumberLinks())
        if(self.pend.getNumberLinks()>prev_DOF):
        # if new links created - change for repeated sliders, let new ones stay at default of 1
            for i in range(0,prev_DOF):
                length_list[i]=(self.length_array[i]).get()
                mass_list[i]=(self.mass_array[i]).get()
        else:
        # if # links reduced or constant - both cases pull until end of new DOF
            for i in range(0,self.pend.getNumberLinks()):
                length_list[i]=(self.length_array[i]).get()
                mass_list[i]=(self.mass_array[i]).get()
        self.pend.setLengths(length_list)
        self.pend.setMasses(mass_list)
        self.prev_var_1 = self.var_1_str.get()
        self.prev_var_2 = self.var_2_str.get()
        self.prev_var_3 = self.var_3_str.get()
        # resimulate (maintain original ICs)
        self.pend.simulate()
        # refresh GUI (including plot axes)
        self.control_panel.pack_forget()
        self.var_frame.pack_forget()
        self.ax.clear()
        new_bounds=np.sum(self.pend.getLengths())
        self.ax.set_xlim(-new_bounds, new_bounds)
        self.ax.set_ylim(-new_bounds, new_bounds)
        self.ax.grid(True)
        tick_locs = np.arange(np.ceil(-new_bounds),np.ceil(new_bounds),1)
        self.ax.set_xticks(tick_locs)
        self.ax.set_yticks(tick_locs)
        self.var_1_ax.set_ylabel(self.prev_var_1)
        self.var_2_ax.set_ylabel(self.prev_var_2)
        self.var_3_ax.set_ylabel(self.prev_var_3)

        # For refreshing variable plots 
        self.letter_1=self.var_1_str.get()[0:2]
        self.num_1=int(self.var_1_str.get()[-1])-1
        if(self.letter_1=="An"):
            self.var_1_line.set_ydata(self.pend.getAngles()[:,self.num_1])
        elif(self.letter_1=="Ve"):
            self.var_1_line.set_ydata(self.pend.getVelocities()[:,self.num_1])
        elif(self.letter_1=="Ac"):
            self.var_1_line.set_ydata(self.pend.getAccelerations()[:,self.num_1])

        self.letter_2=self.var_2_str.get()[0:2]
        self.num_2=int(self.var_2_str.get()[-1])-1
        if(self.letter_2=="An"):
            self.var_2_line.set_ydata(self.pend.getAngles()[:,self.num_2])
        elif(self.letter_2=="Ve"):
            self.var_2_line.set_ydata(self.pend.getVelocities()[:,self.num_2])
        elif(self.letter_2=="Ac"):
            self.var_2_line.set_ydata(self.pend.getAccelerations()[:,self.num_2])

        self.letter_3=self.var_3_str.get()[0:2]
        self.num_3=int(self.var_3_str.get()[-1])-1
        if(self.letter_3=="An"):
            self.var_3_line.set_ydata(self.pend.getAngles()[:,self.num_3])
        elif(self.letter_3=="Ve"):
            self.var_3_line.set_ydata(self.pend.getVelocities()[:,self.num_3])
        elif(self.letter_3=="Ac"):
            self.var_3_line.set_ydata(self.pend.getAccelerations()[:,self.num_3])

        self.add_control_panel(self,0).pack({"side":"left"})    # re-adds control panel
        self.canvas_anim.draw()

    def add_DOF_slider(self,parent):    # control number of pendulum links
        slide_frame=Frame(parent)
        text=Label(slide_frame)
        text["text"]="# DOF"
        text.pack({"side":"left"})
        self.DOF_slider = Scale(slide_frame, from_=1, to=10, orient=HORIZONTAL, resolution=1)
        self.DOF_slider.pack()
        self.DOF_slider.set(self.pend.getNumberLinks())
        return slide_frame

    def add_G_entrybox(self,parent):    # numeric entry box, text validation code based on:
        # http://stackoverflow.com/questions/8959815/restricting-the-value-in-tkinter-entry-widget
        g_box=Frame(parent)
        text=Label(g_box)
        current = self.pend.getGravity()
        text["text"]="Gravity = {}      Change to:".format(current)
        text.pack({"side":"left"})
        vcmd = (parent.register(self.validate),
                '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
        self.G_entrybox = Entry(g_box, validate = 'key', validatecommand = vcmd)
        self.G_entrybox.pack()
        self.G_entrybox.config(width=5)
        return g_box

    def add_L_slider(self,parent,str):  # add length slider for specified link
        slide_frame=Frame(parent)
        text=Label(slide_frame)
        text["text"]="Length {}".format(str)
        text.pack({"side":"left"})
        self.L_slider = Scale(slide_frame, from_=0.5, to=2.0, orient=HORIZONTAL, resolution=0.05)
        self.L_slider.pack()
        self.L_slider.set(self.pend.getLengths()[int(str)-1])
        self.length_array.append(self.L_slider)     # current value can be carried over through re-sim
        return slide_frame

    def add_M_slider(self,parent,str):  # similar to length slider
        slide_frame=Frame(parent)
        text=Label(slide_frame)
        text["text"]="Mass {}".format(str)
        text.pack({"side":"left"})
        self.M_slider = Scale(slide_frame, from_=0.5, to=3.0, orient=HORIZONTAL, resolution=0.05)
        self.M_slider.pack()
        self.M_slider.set(self.pend.getMasses()[int(str)-1])
        self.mass_array.append(self.M_slider)
        return slide_frame

    def add_var_choices(self,parent,initial_run=1):     # create menus to select kinematic variables
        self.var_frame=Frame(parent)
        option_list = []
        for i in range(0,self.pend.getNumberLinks()):
            option_list.append("Angle {}".format(i+1))
            option_list.append("Velocity {}".format(i+1))
            option_list.append("Acceleration {}".format(i+1))
        self.var_1_str=StringVar()
        self.var_2_str=StringVar()
        self.var_3_str=StringVar()
        if(initial_run==1):             # needed since option list refreshes with each re-sim
            self.var_1_str.set(option_list[0])
            self.var_2_str.set(option_list[1])
            self.var_3_str.set(option_list[2])
        else:
            self.var_1_str.set(self.prev_var_1)
            self.var_2_str.set(self.prev_var_2)
            self.var_3_str.set(self.prev_var_3)
        self.var_choice_1 = OptionMenu(self.var_frame,self.var_1_str,*option_list)
        self.var_choice_2 = OptionMenu(self.var_frame,self.var_2_str,*option_list)
        self.var_choice_3 = OptionMenu(self.var_frame,self.var_3_str,*option_list)
        self.var_choice_1.pack()
        self.var_choice_2.pack()
        self.var_choice_3.pack()
        return self.var_frame

    def add_var_plots(self, parent, initial_run=1):     # create animated plots for selected variables
        var_fig = plt.Figure(figsize=(4,4))
        self.var_canvas = FigureCanvasTkAgg(var_fig, master=self)
        
        self.letter_1=self.var_1_str.get()[0:2]  # pull first two characters in string
        self.num_1=int(self.var_1_str.get()[-1])-1      # pull final character in string, make int
        self.var_1_ax = var_fig.add_subplot(311)
        # choose proper variable to plot based on var_i_str data
        if(self.letter_1=="An"):
            self.var_1_line, = self.var_1_ax.plot(self.pend.getT(), self.pend.getAngles()[:,self.num_1])
        elif(self.letter_1=="Ve"):
            self.var_1_line, = self.var_1_ax.plot(self.pend.getT(), self.pend.getVelocities()[:,self.num_1])
        elif(self.letter_1=="Ac"):
            self.var_1_line, = self.var_1_ax.plot(self.pend.getT(), self.pend.getAccelerations()[:,self.num_1])
        self.var_1_dot, = self.var_1_ax.plot(0,0,'ro')
        self.var_1_ax.set_xlabel("t")
        self.var_1_ax.set_ylabel(self.var_1_str.get())

        self.letter_2=self.var_2_str.get()[0:2]
        self.num_2=int(self.var_2_str.get()[-1])-1
        self.var_2_ax = var_fig.add_subplot(312)
        if(self.letter_2=="An"):
            self.var_2_line, = self.var_2_ax.plot(self.pend.getT(), self.pend.getAngles()[:,self.num_2])
        elif(self.letter_2=="Ve"):
            self.var_2_line, = self.var_2_ax.plot(self.pend.getT(), self.pend.getVelocities()[:,self.num_2])
        elif(self.letter_2=="Ac"):
            self.var_2_line, = self.var_2_ax.plot(self.pend.getT(), self.pend.getAccelerations()[:,self.num_2])
        self.var_2_dot, = self.var_2_ax.plot(0,0,'ro')
        self.var_2_ax.set_xlabel("t")
        self.var_2_ax.set_ylabel(self.var_2_str.get())

        self.letter_3=self.var_3_str.get()[0:2]
        self.num_3=int(self.var_3_str.get()[-1])-1
        self.var_3_ax = var_fig.add_subplot(313)
        if(self.letter_3=="An"):
            self.var_3_line, = self.var_3_ax.plot(self.pend.getT(), self.pend.getAngles()[:,self.num_3])
        elif(self.letter_3=="Ve"):
            self.var_3_line, = self.var_3_ax.plot(self.pend.getT(), self.pend.getVelocities()[:,self.num_3])
        elif(self.letter_3=="Ac"):
            self.var_3_line, = self.var_3_ax.plot(self.pend.getT(), self.pend.getAccelerations()[:,self.num_3])
        self.var_3_dot, = self.var_3_ax.plot(0,0,'ro')
        self.var_3_ax.set_xlabel("t")
        self.var_3_ax.set_ylabel(self.var_3_str.get())

        self.num_data=int(len(self.pend.getT()))
        self.var_anim = anim.FuncAnimation(var_fig, self.var_animate, np.arange(0, self.num_data, 30), interval=10, blit=False)
        return self.var_canvas.get_tk_widget()

    def var_animate(self,i):    # to animate red dot on var_plots
        t=i*self.pend.getIncT()

        if(self.letter_1=="An"):
            self.var_1_dot.set_data(t, self.pend.getAngles()[i,self.num_1])
        elif(self.letter_1=="Ve"):
            self.var_1_dot.set_data(t, self.pend.getVelocities()[i,self.num_1])
        elif(self.letter_1=="Ac"):
            self.var_1_dot.set_data(t, self.pend.getAccelerations()[i,self.num_1])

        if(self.letter_2=="An"):
            self.var_2_dot.set_data(t, self.pend.getAngles()[i,self.num_2])
        elif(self.letter_2=="Ve"):
            self.var_2_dot.set_data(t, self.pend.getVelocities()[i,self.num_2])
        elif(self.letter_2=="Ac"):
            self.var_2_dot.set_data(t, self.pend.getAccelerations()[i,self.num_2])

        if(self.letter_3=="An"):
            self.var_3_dot.set_data(t, self.pend.getAngles()[i,self.num_3])
        elif(self.letter_3=="Ve"):
            self.var_3_dot.set_data(t, self.pend.getVelocities()[i,self.num_3])
        elif(self.letter_3=="Ac"):
            self.var_3_dot.set_data(t, self.pend.getAccelerations()[i,self.num_3])
        
        return self.var_1_line, self.var_2_line, self.var_3_line

    def __AnimInit(self):       # initializer for x-y anim_plot
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text

    def __animate(self, ii):    # repeated function for x-y anim_plot
        xx = [0]; yy = [0]
        for jj in range(self.pend.getNumberLinks()):
            xx.append(self.pend.getXYPositions()[ii][jj][0])
            yy.append(self.pend.getXYPositions()[ii][jj][1])
        self.line.set_data(xx,yy)
        self.time_text.set_text(self.time_template%(ii*self.pend.getIncT()))
        return self.line, self.time_text

    def add_anim_plot(self, parent):    # x-y plot (visual sim.) of pendulum
        bounds = np.sum(self.pend.getLengths())
        self.simfig = plt.figure(figsize=(4,4))
        self.canvas_anim = FigureCanvasTkAgg(self.simfig, master=self)
        self.ax = self.simfig.add_subplot(111, autoscale_on=False, xlim=(-bounds, bounds), ylim=(-bounds, bounds))
        lims=np.arange(np.ceil(-bounds),np.ceil(bounds),1)
        self.ax.set_xticks(lims)
        self.ax.set_yticks(lims)
        self.ax.grid()
        self.line, = self.ax.plot([], [], 'o-', lw=2)
        self.time_template = 'time = %.1fs'
        self.time_text = self.ax.text(0.05, 0.9, '', transform=self.ax.transAxes)
        self.xy_anim = anim.FuncAnimation(self.simfig, self.__animate, np.arange(1, len(self.pend.getT()),
            int(0.03/self.pend.getIncT())), interval=12.5, blit=True, init_func = self.__AnimInit)
        return self.canvas_anim.get_tk_widget()

    def validate(self, action, index, value_if_allowed,
                       prior_value, text, validation_type, trigger_type, widget_name):
    # used to limit text entry in TK Entry objects (gravity)
        if text in '0123456789.-+':
            try:
                float(value_if_allowed)
                return True
            except ValueError:
                return False
        else:
            return False