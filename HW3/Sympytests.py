import sympy as s
import numpy as np

n = 2
t = s.symbols('t')
temp = s.symbols('temp')
temp2 = s.symbols('temp2')

theta = s.Function('theta_1')

expressions = []	#debug item
x_expr = 0
y_expr = 0
pe_expr = 0
ke_expr = 0

ii = 0
while True:		#This loop iteratively builds the KE and PE expressions based on the # of links
	ii += 1
	if ii > n:
		break
	theta = s.Function('theta_%i'%(ii))
	l,m,g = s.symbols('l%i,m%i,g%i'%(ii,ii,ii))
	x_expr += l*s.sin(theta(t))
	y_expr += -l*s.cos(theta(t))
	pe_expr += m*g*y_expr
	x_dot_expr = x_expr.diff(t)
	y_dot_expr = y_expr.diff(t)
	v_sq_expr = x_dot_expr**2 + y_dot_expr**2
	ke_expr += 0.5*m*v_sq_expr

	# print "Velocity: ", v_sq_expr
	# print x_dot_expr

	expressions.append([x_expr,y_expr,pe_expr,ke_expr])		#debug item

ke_expr = ke_expr.simplify()
l_expr = ke_expr - pe_expr

print "P.E.: ", pe_expr		#debug items
print "K.E.: ", ke_expr
print "Lagrangian: ", l_expr

expressions = []

euler_lagrange = []
ddots = []

ii = 0
while True:		# this loop iteratively builds the Euler-Lagrange differential equations for each angle
	ii += 1
	if ii > n:
		break
	theta = s.Function('theta_%i'%(ii))
	l_dot_expr = l_expr.subs(s.Derivative(theta(t), t), temp).diff(temp).subs(temp,s.Derivative(theta(t), t))
	# In the above expression we substitute the derivative function for a symbol, execute the partial, then replace the symbol with the differential function
	l_ddot_expr = l_dot_expr.diff(t)
	l_theta_expr = l_expr.subs(s.Derivative(theta(t),t),temp2).subs(theta(t),temp).diff(temp).subs(temp,theta(t))
	el_de = s.Eq(l_ddot_expr,l_theta_expr)
	print "dL/dTheta_dot: ", l_dot_expr 		#debug items
	print "dL/dTheta_ddot: ", l_ddot_expr
	print "dL/dTheta: ", l_theta_expr

	expressions.append([l_dot_expr,l_ddot_expr,l_theta_expr,el_de])
	euler_lagrange.append(el_de.simplify())
	ddots.append(s.Derivative(theta(t),t,t))

print euler_lagrange
print ddots
print "Solution:", s.solve(euler_lagrange,ddots) 
#solves Euler-Lagrange diff eqs. HOWEVER, only proven for 2 pendulum links... complexity/calculation time exponentially explodes beyond two links

#----simplifies/clarifies equations; for debugging----
thdot1, thdot2, thddot1, thddot2 = s.symbols('thdot1, thdot2, thddot1, thddot2')
theta = s.Function('theta_1')
new = expressions[0][2].subs(s.Derivative(theta(t),t), thdot1)
new = new.subs(s.Derivative(theta(t),t,t), thddot1)
theta = s.Function('theta_2')
new = new.subs(s.Derivative(theta(t),t), thdot2)
new = new.subs(s.Derivative(theta(t),t,t), thddot2)

# theta = s.Function('theta_%i' % n)
# expr.subs(theta, 5)
# expr.subs([(theta_1, 45), (theta_2, 30)])
# expr.evalf()
