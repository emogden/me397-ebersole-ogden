# Pendulum Equations adapted from:
# ENERGY EQUATIONS FOR N - TUPPLE PENDULA SYSTEM by
# Sakwa, Prichani, Ayodo, Sarai, Omondi, and Wafula at
# http://www.cibtech.org/J%20PHYSICS%20MATHEMATICAL%20SCIENCES/PUBLICATIONS/2012/Vol%202%20No.%202/18-JPMS-25%20Thomas%20sakwa.pdf

from numpy import sin, cos, pi
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.animation as animation

class Pendulum:
	# constructor
	def __init__(self):
		self.__grav = -9.81
		self.__n = 1
		self.__lengths = np.ones(self.__n)
		self.__masses = np.ones(self.__n)
		self.__init_angles = np.zeros(self.__n)
		self.__init_vels = np.zeros(self.__n)
		self.__inc_t = 0.001
		self.__end_t = 20
		self.__t = np.arange(0.0,self.__end_t,self.__inc_t)
		self.__sim_angles = np.zeros([1,self.__n])
		self.__sim_vels = np.zeros([1,self.__n])
		self.__sim_accels = np.zeros([1,self.__n])
		self.__sim_pos = np.zeros([1,self.__n,2])

	def copy_values(self, pend): 		# copy "constructor"; private-ness violated
		self.__grav = pend.__grav
		self.__n = pend.__n
		self.__lengths = pend.__lengths
		self.__masses = pend.__masses
		self.__init_angles = pend.__init_angles
		self.__init_vels = pend.__init_vels
		self.__inc_t = pend.__inc_t
		self.__end_t = pend.__end_t
		self.__t = pend.__t
		self.__sim_angles = pend.__sim_angles
		self.__sim_vels = pend.__sim_vels
		self.__sim_accels = pend.__sim_accels
		self.__sim_pos = pend.__sim_pos

	def simulate(self):					# empirical simulation 
		self.__sim_angles[0] = self.__init_angles
		self.__sim_vels[0] = self.__init_vels
		for jj in range(self.__n):		# intialize positions
			self.__sim_pos[0][jj][0] = self.__lengths[jj]*sin(self.__sim_angles[0][jj])
			self.__sim_pos[0][jj][1] = -self.__lengths[jj]*cos(self.__sim_angles[0][jj])
		n = self.__n - 1
		accels = np.zeros([1,self.__n])
		vels = np.zeros([1,self.__n]); vels[0] = self.__sim_vels[0]
		angs = np.zeros([1,self.__n]); angs[0] = self.__sim_angles[0]
		pos = np.zeros([1,self.__n,2]); pos[0] = self.__sim_pos[0]
		print "Calculating . . ."

		for ii in range(1,int(self.__end_t/self.__inc_t)):
			# If more than a double pendulum is requested, use the following eqs.. note that these
			# 	perform poorly at large angles
			if self.__n > 2:
				# Sakwa et al. Eq. 19
				accels[0][n] = -((self.__masses[n-1]+self.__masses[n])
					/self.__masses[n-1]*(self.__sim_angles[ii-1][n-1]
					-self.__sim_angles[ii-1][n]))*self.__grav/self.__lengths[n]
				# Sakwa et al. Eq. 13
				accels[0][0] = -((self.__masses[1]/self.__masses[0])*self.__sim_angles[ii-1][1]
					-(self.__masses[0]+self.__masses[1])/self.__masses[0]
					*self.__sim_angles[ii-1][0])*self.__grav/self.__lengths[0]
				for jj in range(1,n):
					# Sakwa et al. Eq. 18
					accels[0][jj] = -(np.sum(self.__masses[jj-1:])/self.__masses[jj-1]
						*self.__sim_angles[ii-1][jj-1]-(self.__masses[jj-1]
						+self.__masses[jj])/self.__masses[jj-1]*(np.sum(self.__masses[jj:])
						/self.__masses[jj])*self.__sim_angles[ii-1][jj]
						+np.sum(self.__masses[jj+1:])/self.__masses[jj]
						*self.__sim_angles[ii-1][jj+1])*self.__grav/self.__lengths[jj]
			# Double Pendulum eqs, based on matplotlib sample animation code			
			elif self.__n > 1:
				del_ = self.__sim_angles[ii-1][1]-self.__sim_angles[ii-1][0]
				M1 = self.__masses[0]; M2 = self.__masses[1]
				L1 = self.__lengths[0]; L2 = self.__lengths[1]
				den1 = (M1+M2)*L1 - M2*L1*cos(del_)*cos(del_)
				den2 = (L2/L1)*den1
				accels[0][0] = (M2*L1*self.__sim_vels[ii-1][0]**2*sin(del_)*cos(del_) 
					- M2*self.__grav*sin(self.__sim_angles[ii-1][1])*cos(del_) 
					+ M2*L2*self.__sim_vels[ii-1][1]**2*sin(del_) 
					+ (M1+M2)*self.__grav*sin(self.__sim_angles[ii-1][0]))/den1
				accels[0][1] = (-M2*L2*self.__sim_vels[ii-1][1]**2*sin(del_)*cos(del_) 
					- (M1+M2)*self.__grav*sin(self.__sim_angles[ii-1][0])*cos(del_) 
					- (M1+M2)*L1*self.__sim_vels[ii-1][0]**2*sin(del_) 
					+ (M1+M2)*self.__grav*sin(self.__sim_angles[ii-1][1]))/den2
			# Single pendulum
			else:
				accels[0][0] = sin(self.__sim_angles[ii-1][0])*self.__grav/self.__lengths[0]
			# Simple manual Euler solver
			for jj in range(self.__n):
				vels[0][jj] += accels[0][jj]*self.__inc_t
				angs[0][jj] += vels[0][jj]*self.__inc_t

				pos[0][jj][0] = self.__lengths[jj]*sin(angs[0][jj])
				pos[0][jj][1] = -self.__lengths[jj]*cos(angs[0][jj])
				if jj > 0:
					pos[0][jj][0] += pos[0][jj-1][0]
					pos[0][jj][1] += pos[0][jj-1][1]
			
			self.__sim_accels = np.append(self.__sim_accels,accels,0)
			self.__sim_vels = np.append(self.__sim_vels,vels,0)
			self.__sim_angles = np.append(self.__sim_angles,angs,0)
			self.__sim_pos = np.append(self.__sim_pos,pos,0)

	def setNumberLinks(self,n):
		self.__n = n 
		self.__sim_angles = [np.zeros(self.__n)]
		self.__sim_vels = [np.zeros(self.__n)]
		self.__sim_accels = [np.zeros(self.__n)]
		self.__sim_pos = [np.zeros([self.__n,2])]

		# If the number of links increases, add placeholder 1 values to parameters
		if n > len(self.__masses):
			self.__masses = np.append(self.__masses,np.ones(n-len(self.__masses)))
			self.__lengths = np.append(self.__lengths,np.ones(n-len(self.__lengths)))
			self.__init_vels = np.append(self.__init_vels,np.zeros(n-len(self.__init_vels)))
		# If link number decreases, trim parameter lists
		elif n < len(self.__masses):
			self.__masses = self.__masses[:(n-len(self.__masses))]
			self.__lengths = self.__lengths[:(n-len(self.__lengths))]
			self.__init_vels = self.__init_vels[:(n-len(self.__init_vels))]
		self.__init_angles = []
		# Initialize starting angles at pi/n. This keeps pendulums of n > 2 near small angle values, so that 
		#	the equations used above are more valid. A pendulum of 1 link starts slightly below pi so that it does
		# 	not remain stationary
		if n == 1:
			self.__init_angles = [99*pi/100]
		else:
			for ii in range(n):
				self.__init_angles.append(pi/n)

	# set functions
	def setAngleICs(self,angles):
		self.__init_angles = angles

	def setVelocityICs(self,velocities):
		self.__init_vels = velocities 

	def setMasses(self,masses):
		self.__masses = masses

	def setLengths(self,lengths):
		self.__lengths = lengths

	def setGravity(self, grav):
		self.__grav = grav

	# get functions
	def getXYPositions(self):
		return self.__sim_pos

	def getVelocities(self):
		return self.__sim_vels

	def getAngles(self):
		return self.__sim_angles

	def getAccelerations(self):
		return self.__sim_accels

	def getNumberLinks(self):
		return self.__n

	def getGravity(self):
		return self.__grav

	def getLengths(self):
		return self.__lengths

	def getMasses(self):
		return self.__masses

	def getIncT(self):
		return self.__inc_t

	def getT(self):
		return self.__t