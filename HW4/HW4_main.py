from RobotSimulation import *
# setup the 3-link floating robot
import RobotSetup as rs

__version__ = "1.0.0"

# initial conditions
q0 = np.array([0.0,0.04,0.0,-np.pi/10,np.pi/6,-np.pi/12])
qdot0 = np.array([.5,.1,0,0.5,1,0])

# instantiate and run the simulation
sim = RobotSimulation(0.001,rs.legRobot,rs.legRobotForces)
sim.initialize(q0,qdot0)
sim.step(300)
sim.animate()
