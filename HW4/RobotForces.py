from SerialRobot import *
from math import sqrt, fabs
from Shape import *
import abc

__version__ = "1.0.0"

#base class to encapsulate configuration-dependent forces on the robot

class RobotForce:

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def __init__(self, name_):
		self.name = name_
		self.F = []
		self.J = []

	# getFroce/Jacobian not made abstract methods, since base implementation acceptable
	def getForce(self):
		return self.F

	def getJacobian(self):
		return self.J

	@abc.abstractmethod
	def update(self,robot_=SerialRobot("default")):
		print "NOT IMPLEMENTED"
		pass

#external contact forces via penalty method with damping
#for now this is explicit for circles and half-planes
class ExternalContact(RobotForce):
	def __init__(self, name_, planePoint_, planeNormal_, K_, D_, jointIdx_,
				 robot_=SerialRobot("default")):
		RobotForce.__init__(self,name_)
		self.K = K_
		self.D = D_
		self.point = planePoint_
		self.normal = planeNormal_
		self.idx = jointIdx_
		if robot_.ndofs>0:
			self.__setup(robot_)
			self.update(robot_)
		else:
			"WARNING: CONTACT FORCE ON BLANK ROBOT"

	#setup containers for forces and associated Jacobians
	def __setup(self,robot_=SerialRobot("default")):
		#Get the list of collision objects associated with this frame
		collisions = robot_.getLink(self.idx).collisions
		# Expect a 2d force for each circle/plane collision
		# (more for vertex-based collisions)
		for ii in range(0,len(collisions)):
			self.F.append(np.zeros(2))
			self.J.append(np.zeros((2,robot_.ndofs)))
		self.normal = self.normal/np.linalg.norm(self.normal)
		self.vis = Line(self.name,np.array([0,0]),np.array([0,0]))

	#Updates forces/Jacobians for robot_ (already updated) to current position
	def update(self,robot_=SerialRobot("default")):
		#Get collisions and current transform
		collisions = robot_.getLink(self.idx).collisions
		T = robot_.getTransformAsMatrix(self.idx)
		#For each collision....
		for ii in range(0,len(collisions)):
			# update the collision
			collisions[ii].updateFromMatrix(T)
			# calculate the (signed) distance from the circle center to the plane
			center = collisions[ii].center
			w = (center-self.point).squeeze()
			signed_distance = np.dot(self.normal, w)
			# calculate the (signed) distance from the edge of the circle to the plane
			d = signed_distance - collisions[ii].radius
			if(signed_distance <= 0): #contact has occured!
				# calculate the velocity of the point
				center_J = collisions[ii].T0.getInverseSE2()[0:2,2] # in joint frame
				J = robot_.getJacobian(self.idx,center_J)[1:,:]
				v_point = np.dot(J,robot_.qdot)
				# set penalty-based force F = K*self.normal*|d| - D*v_point
				# self.F[ii] = self.K*-self.normal*abs(d) + self.D*v_point
				self.F[ii] = self.K*-self.normal*abs(signed_distance) + self.D*v_point
				# update Jacobian
				self.J[ii] = J
			else: #no contact
				self.F[ii] = np.zeros(2)
				self.J[ii] = np.zeros((2,robot_.ndofs))
			
#elastic muscles 
class LinearMuscle(RobotForce):
	def __init__(self,name_,K_,joint1Idx_,point1_,joint2Idx_,point2_,preloadDelta_,
				 robot_=SerialRobot("default")):
		RobotForce.__init__(self,name_)
		self.K = K_
		self.idx1 = joint1Idx_
		self.p1 = point1_
		self.idx2 = joint2Idx_
		self.p2 = point2_
		self.L0 = 0
		self.mag = 0
		self.dir = np.zeros(2)
		self.delta = preloadDelta_
		if robot_.ndofs>0:
			self.__setup(robot_)
			self.update(robot_)
		else:
			"WARNING: MUSCLE FORCE ON BLANK ROBOT"

	def __setup(self, robot_=SerialRobot("default")):
		#Treating muscles as external forces gives equal and opposite forces on 2 joints
		for ii in range(0,2):
			self.F.append(np.zeros(2))
			self.J.append(np.zeros((2,robot_.ndofs)))
		p1_inertial = robot_.getPoint(self.idx1,self.p1)
		p2_inertial = robot_.getPoint(self.idx2,self.p2)
		d0 = p2_inertial - p1_inertial
		L0 = sqrt(d0[0]**2 + d0[1]**2)
		self.L0 = fabs(L0-self.delta)
		self.vis = Line(self.name,p1_inertial,p2_inertial)

	def update(self,robot_):
		# Calculate vector from anchor point1 to anchor point2
		p1_inertial = robot_.getPoint(self.idx1,self.p1)
		p2_inertial = robot_.getPoint(self.idx2,self.p2)
		self.vis = Line(self.name,p1_inertial,p2_inertial)		
		d0 = p1_inertial - p2_inertial
		self.L0 = abs(np.linalg.norm(d0) - self.delta)

		# Calculate magnitude of spring force given initial pre-load
		self.mag = self.L0*self.K

		# Calculate and update spring forces and Jacobians
		#linear jacobians at anchor points
		self.J[0] = robot_.getJacobian(self.idx1,self.p1)[1:]
		self.J[1] = robot_.getJacobian(self.idx2,self.p2)[1:]
		#In inertial frame - check dir's of forces!
		self.F[0] = self.mag*-d0/np.linalg.norm(d0)
		self.F[1] = self.mag*d0/np.linalg.norm(d0)
