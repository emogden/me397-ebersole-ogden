import numpy as np
from numpy import cos, sin, pi
import matplotlib.pyplot as plt
import matplotlib.path as pth
import matplotlib.patches as patches
import matplotlib.animation as anim
import abc

from Transform import *

__version__ = "1.0.0"

#Base class for shapes
#All shapes are defined w.r.t. a link (parent joint) frame, 
#with an initial (static) rigid body transform (T0) relative to that frame
class Shape:

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def __init__(self,name_,transform_=Transform(np.zeros(2),0.0)):
		self.name = name_
		self.T0 = transform_

	#Not abstracted, since sufficient for subclasses
	def getStaticTransform(self):
		return self.T0

	#update internal variables from a transform object
	#baseTransform maps the parent frame to the inertial frame
	@abc.abstractmethod
	def update(self, baseTransform_=Transform(np.zeros(2),0.0)):
		#updates internal variables in preparation for drawing a figure
		print "UPDATE NOT IMPLEMENTED"
		pass

	#update internal variables from a transform represented as a matrix
	#baseTransform maps the parent frame to the inertial frame
	@abc.abstractmethod
	def updateFromMatrix(self, baseTMatrix_):
		#updates internal variables in preparation for drawing a figure
		print "UPDATE FROM MATRIX NOT IMPLEMENTED"
		pass

	#draw adds the object (e.g. a patch) to the axis ax_
	@abc.abstractmethod
	def draw(self, ax_):
		#draws the object
		print "DRAW NOT IMPLEMENTED"
		pass

#Line from p1 to p2
class Line(Shape):
	def __init__(self,name_,p1_,p2_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)
		#initialize member data
		self.p1 = p1_
		self.p2 = p2_
		#initial vertices
		self.v0 = np.array([p1_,p2_])
		#fully transformed vertices
		self.vertices = np.array([p1_,p2_])

		self.codes = [pth.Path.MOVETO,pth.Path.LINETO]
		self.line_path = pth.Path(self.vertices,self.codes)

		self.updateFromMatrix(np.identity(3))


	def setMatrixFetchFunction(self, matrix_fetcher):
		# add a matrix_fetcher member, which is a function
		self.fetch_matrix=matrix_fetcher

	def animate(self):
		# call matrix_fetcher as a function
		self.updateFromMatrix(self.fetch_matrix())

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		baseT = baseTransform_.getInverseSE2()
		self.updateFromMatrix(baseT)

	def updateFromMatrix(self, baseTMatrix_):
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		self.vertices[0] = np.dot(T0[0:2,0:2],self.p1) + T0[0:2,2]
		self.vertices[1] = np.dot(T0[0:2,0:2],self.p2) + T0[0:2,2]

	def draw(self, ax_, color_='r',lw_=2):
		#Note: use of patch not completely necessary...but useful hint for rectangle implementation
		self.patch = patches.PathPatch(self.line_path,color=color_,lw=lw_)
		ax_.add_patch(self.patch)

#Rectangle centered at (0,0) with width in x-direction and height in y-direction
class Rectangle(Shape):
	def __init__(self,name_,width_,height_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)

		self.width = width_
		self.height = height_
		h2 = height_*0.5
		w2 = width_*0.5
		self.v0 = np.array([[w2,-w2,-w2,w2,w2],[h2,h2,-h2,-h2,h2]])
		self.vertices = np.zeros((5,2))

		self.codes = np.ones(5,int) * pth.Path.LINETO
		self.codes[0] = pth.Path.MOVETO
		self.codes[4] = pth.Path.CLOSEPOLY
		self.rectangle_path=pth.Path(self.vertices, self.codes)
		self.updateFromMatrix(np.identity(3))

	def setMatrixFetchFunction(self, matrix_fetcher):
		# add a matrix_fetcher member, which is a function
		self.fetch_matrix=matrix_fetcher

	def animate(self):
		# call matrix_fetcher as a function
		self.updateFromMatrix(self.fetch_matrix())

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		baseT = baseTransform_.getInverseSE2()
		self.updateFromMatrix(baseT)

	def updateFromMatrix(self, baseTMatrix_):
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		for i in range(0,5):
			self.vertices[i,:] = (T0[0:2,0:2].dot(self.v0[:,i]) + T0[0:2,2]).T # .T >> Transpose

	def draw(self, ax_, fcolor_='r', ecolor_='k', lw_=2, alpha_=0.5):
		self.patch = patches.PathPatch(self.rectangle_path,facecolor=fcolor_, edgecolor=ecolor_, alpha=alpha_)
		ax_.add_patch(self.patch)

#Circle of radius radius_ centered at (0,0)
class Circle(Shape):
	def __init__(self,name_,radius_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)
		self.radius = radius_
		self.center = np.zeros(2)
		self.updateFromMatrix(np.identity(3))


	def setMatrixFetchFunction(self, matrix_fetcher):
		# add a matrix_fetcher member, which is a function
		self.fetch_matrix=matrix_fetcher

	def animate(self):
		# call matrix_fetcher as a function
		self.updateFromMatrix(self.fetch_matrix())

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		baseT = baseTransform_.getInverseSE2()
		self.updateFromMatrix(baseT)

	def updateFromMatrix(self, baseTMatrix_):
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		self.center[:] = T0[0:2,2]

	def draw(self, ax_, fcolor_='b', ecolor_='k', lw_=2, alpha_=0.5):
		self.patch = patches.Circle(self.center,self.radius,facecolor=fcolor_, edgecolor=ecolor_, alpha=alpha_)
		ax_.add_patch(self.patch)

# Debug Items
if __name__=='__main__':
	
	# fig = plt.figure()
	# ax = fig.gca()
	# A = Line('first_line',np.array([0,0]), np.array([1,1]),Transform(np.array([0.5,0]),np.pi/6))
	# B = Circle('first_circle', 0.2,Transform(np.array([0.5,0.5]),0))
	# C = Rectangle('first_rectangle',0.6,0.2,Transform(np.array([0.5,0.5]),np.pi/2))
	# A.draw(ax)
	# B.draw(ax,'b')
	# C.draw(ax)
	# plt.show()

	print "hello world"
	fig, ax = plt.subplots()
	ax.axis('equal')
	ax.axis([-1,1,-1,1])
	line=Line("line 1", [0.2,0.5],[0,0])
	rect=Rectangle("rect 1", 0.4, 0.4, transform_ = Transform(np.zeros(2),pi/6))
	rect2=Rectangle("rect 2", 0.8,0.1)	
	circ = Circle("circ 1", 0.2) #, transform_ = Transform(np.array([0.5,0]),0))
	rect.draw(ax)
	rect2.draw(ax)
	circ.draw(ax)
	line.draw(ax)
	trans=np.array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]])
	trans2=np.array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]])	
	trans3=np.array([[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]])	
	fetch_transform_lambda = lambda : trans   			#this is an inline function
#					add_1 = lambda x : x+1
#					add_1(2) # should be 3
	rect.setMatrixFetchFunction(fetch_transform_lambda)
	rect2.setMatrixFetchFunction(lambda : trans2)
	circ.setMatrixFetchFunction(lambda : trans3)
	line.setMatrixFetchFunction(lambda : trans3)
	animateables=[line, rect, rect2, circ]

	def pose_objects(i):
		theta=i*0.05
		x = 0.0 + 0.3*np.sin(i*0.15)
		y = 0.0 + 0.3*np.cos(i*0.1)
		x2 = 0.0 - 0.3*np.sin(i*0.2)
		y2 = 0.0 + 0.3*np.cos(i*0.1)
		x3 = 0.3*np.sin(-i*0.1)
		y3 = 0.3*np.cos(-i*0.099)
		trans[:] = Transform(np.array([x,y]),theta).getInverseSE2() # [:] allows for persistence
		trans2[:] = Transform(np.array([x2,y2]),-theta).getInverseSE2() # [:] allows for persistence
		trans3[:] = Transform(np.array([x3,y3]),-theta).getInverseSE2() # [:] allows for persistence		
		# trans[0:2,0:3]=np.array([
		# 	[cos(theta),-sin(theta),x],
		# 	[sin(theta),cos(theta),y]
		# 	])

	# obtain the threading and time libraries for starting
	# new threads and pausing on the real clock.

	import time
	import thread

	# define a number which loop poses can use to remember
	# where it is in the list of poses
	pose_number = 0

	# define a function which iterates over the poses using
	# the pose_number variable like a global

	def loop_poses():
		# pose_number cannot be modified through side effects
		global pose_number
		if pose_number >= 1000:
			pose_number = 0
		pose_objects(pose_number)
		pose_number+=1

	# define a funciton which is the main funciton for the new
	# thread, calling loop_poses and waiting briefly
	def loop_poses_forever():
		while True:
			loop_poses()
			time.sleep(0.02)

	# this is the call which starts your new thread. The ()
	# means you don't need to pass any arguments to 
	# loop_poses_forever
	thread.start_new_thread(loop_poses_forever,())		

	def animate(i):
		# loop_poses()

		for animateable in animateables:
			animateable.animate()
		# rect.animate()
	ani=anim.FuncAnimation(fig, animate, range(0,1000), interval = 20)

	# plt.show is unlike normal theads in that it halts your execution
	plt.show()



