#Set of classes to enable dynamic simulations of serial mult-body
#systems in 2d for HW4 of ME397
import numpy as np

__version__ = "1.0.0"

#Encapuslation of a 2d rigid body transform from F0 to F1
#d_ is offset from F0 to F1, represented in F0
#theta_ parametrizes rotation from F0 to F1 
#s.t. x_1 {represented in F1} = R(theta_) x_0
class Transform:
	def __init__(self,d_=np.zeros(2),theta_=0.0):
		# copy initial data to local variables
		self.d = d_.squeeze()
		self.theta = theta_
		# Setup appropriately sized np.arrays for other variabls.
		self.R = np.zeros((2,2))
		# transforms a point represented in F1 
		# to its equivalent representation in F0
		self.T_1to0 = np.zeros((3,3))
		self.T_1to0[2,2] = 1.0
		# transforms a point represented in F0 
		# to its equivalent representation in F1
		self.T_0to1 = np.zeros((3,3))		
		self.T_0to1[2,2] = 1.0
		# Call update to initialize transforms
		self.__update()

	def __update(self):
		# pass
		#set rotation matrix R(theta)
		self.R[0][0] = self.R[1][1] = np.cos(self.theta)
		self.R[0][1] = np.sin(self.theta)
		self.R[1][0] = -np.sin(self.theta)
		self.R_inv = np.linalg.inv(self.R)
		self.d_tf = np.dot(self.R,-self.d)

		#set self.T_1to0 
		self.T_1to0[0:2,0:2] = self.R_inv
		self.T_1to0[0:2,2] = self.d

		#set self.T_0to1
		self.T_0to1[0:2,0:2] = self.R
		self.T_0to1[0:2,2] = self.d_tf

	def updateTheta(self,theta_):	
		self.theta = theta_
		self.__update()

	def updateDisplacement(self,d_):
		self.d = d_.squeeze()
		self.__update()

	def getSE2(self):
		return self.T_0to1

	#This is most commonly used to transform joint frame to inertial frame
	def getInverseSE2(self):
		return self.T_1to0

# Debug Items
if __name__=='__main__':	
	disp = np.array([2,0])
	test = Transform(disp,np.pi/4)
	tf =  test.getSE2()
	tf_inv = test.getInverseSE2()
	print tf
	print tf_inv

	point = np.array([[3],[1]])
	point2 = np.array([[-2],[0]])
	augmented_point=np.ones((3,1))
	augmented_point[0:2]=point
	print augmented_point
	new_aug_point=np.dot(tf,augmented_point)
	print new_aug_point[0:2,0]