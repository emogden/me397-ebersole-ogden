import zmq
import sys
import time
import numpy as np
import text_processing as tp

__version__ = "0.7.0"

class GreenReactor:
    def __init__(self, port_speech, port_opinion, ip_addr = "127.0.0.1"):
        self.__port_speech = str(port_speech)
        self.__port_opinion = str(port_opinion)
        self.__ip_addr = str(ip_addr)

        keywords_bad = ['y\'all','python','hedge','star wars']
        keywords_good = ['yous guys','c++','shrubbery','star trek']
        # keywords_good = ['dream','nation','freedom','justice']
        # keywords_bad = ['negro','cannot','injustice','slaves']
        self.processor = tp.TextProcessing(keywords_good,keywords_bad)

        # reactor opinion stats
        self.opinion = 0
        self.pronoun_op = 0
        self.keyword_op = 0
        self.wl_setpoint = 5.1
        self.wps_setpoint = 17

        self.words = []
        self.multi_words = ''
        self.word_1 = ''
        self.word_2 = ''

        self.listen()

    def listen(self):
        context = zmq.Context()

        # creating socket to the account
        socket_speech = context.socket(zmq.SUB)
        socket_speech.setsockopt(zmq.SUBSCRIBE, '')
        # connecting to the publisher
        socket_speech.connect ("tcp://"+self.__ip_addr + ":" + self.__port_speech)

        # zmq communications
        socket_opinion = context.socket(zmq.PUB)
        # binding to all ip addresses
        socket_opinion.bind("tcp://*:%s" % self.__port_opinion)

        # polling mechanism
        poller = zmq.Poller()
        poller.register(socket_speech, zmq.POLLIN)
        print "listening..."

        while True:
            try:
                # polling with number indicting the wait in ms
                socks = dict(poller.poll())
                if socket_speech in socks and socks[socket_speech] == zmq.POLLIN:
                    self.words = socket_speech.recv().split()
                    # word_ = socket_speech.recv()
                    self.processor.words_total += len(self.words)
                for ii in self.words:
                    self.word_2 = ii
                    if self.word_1:
                        self.multi_words = self.word_1 + ' ' + self.word_2
                    else:
                        self.multi_words = self.word_2
                    self.processor.words_local += 1       # since called on a per-word basis
                    self.__text_processing(ii,self.multi_words)
                    self.__opinion_metrics()
                    socket_opinion.send("%f" % (self.opinion))
                    self.word_1 = self.word_2
                # print self.opinion
            except KeyboardInterrupt:
                print "stopping..."
                print "Average words per sentence:", self.processor.wps_avg
                print "Max words per sentence:", self.processor.wps_max
                print "Min words per sentence:", self.processor.wps_min
                print "Average word length:", self.processor.wl_avg
                print "Max word length:", self.processor.wl_max
                print "Min word length:", self.processor.wl_min
                print "Most common pronoun:", self.processor.pronoun_sorted[0], \
                        "occured", self.processor.pronoun_count[0], "times"
                print "Second most common pronoun:", self.processor.pronoun_sorted[1], \
                        "occured", self.processor.pronoun_count[1], "times"
                break


    def __opinion_metrics(self):
        if self.processor.wps_avg:
            o_sen_lengths = 10*(self.processor.wps_avg - self.wps_setpoint) #+ (self.wps_max + self.wps_min - self.wps_setpoint)
        else:
            o_sen_lengths = 0
        o_word_lengths = 10*(self.processor.wl_avg - self.wl_setpoint) #+ (self.wl_max + self.wl_min - self.wl_setpoint)
        self.opinion = 2*self.processor.keyword_op + o_word_lengths + o_sen_lengths + self.processor.pronoun_op
        self.opinion = 100*(np.arctan(self.opinion/100)/np.pi + 0.5) # This adjusts the opinion value to a more meaningful approval rating
        # print self.keyword_op, o_word_lengths, o_sen_lengths, self.pronoun_op

    def __text_processing(self,word,multiword):
            # keep logic to prevent both keyword and pronoun check?
            word = self.processor.punctuation_check(word)
            # if a word is both a keyword and a pronoun, keyword status
            # takes precedence (does not double-count)
            if not self.processor.keyword_check(word,multiword):
                self.processor.pronoun_check(word)
            self.processor.word_length(word)
            self.processor.words_per_sentence(word)



if __name__ == "__main__":
    ip = "127.0.0.1"
    port_sp = 5556
    port_op = 5557
    if len(sys.argv) > 1:
        ip =  str(sys.argv[1])
        if len(sys.argv) > 2:
            port_op = str(sys.argv[2])
            if len(sys.argv) > 3:
                port_sp = str(sys.argv[3])
                if len(sys.argv) > 4:
                    print "Too many arguments!! Usage: python green.py [ip_addr] [opinion port] [speech port]"
                    exit()

    gr = GreenReactor(port_sp,port_op,ip)