import sys
import time
import numpy as np

__version__ = "0.7.0"

# This class does all the text processing for the green and purple reactors
class TextProcessing:
    def __init__(self, kw_good, kw_bad, collective_pronouns = True):

        self.keywords_bad = kw_bad
        self.keywords_good = kw_good
        self.keywords = self.keywords_good + self.keywords_bad
        self.keyword_tracker = {}
        self.keyword_sorted = ['','']
        self.keyword_count = [0,0]
        self.keyword_op = 0

        #currently only personal, possesive; consider adding demonstrative (this/those/etc)
        #relative pronounds (whoever/whatever/etc), indefinite pronouns (everybody/anybody/etc)
        #intensive pronouns (myself/yourself/etc), interrogative pronouns (who/which/etc)
        #reciprocal pronouns (each other/one another/etc) [can't do with single-word checks]
        #include 'it' in the list?
        #you is both singular and plural - how to treat this? currently all in singular
        # "I" is lowercase for word/pronoun matching
        self.pronouns_sing = ['i','me','you','he','him','she','her',
                            'mine','my','your','his','her','yours','hers']
        self.pronouns_plur = ['we','us','our','ours','they','them','their','theirs']
        if collective_pronouns == True:
            self.pronouns_good = self.pronouns_plur
            self.pronouns_bad = self.pronouns_sing
        else:
            self.pronouns_good = self.pronouns_sing
            self.pronouns_bad = self.pronouns_plur

        self.pronouns = self.pronouns_sing + self.pronouns_plur
        self.pronoun_tracker = {}
        self.pronoun_sorted = ['','']
        self.pronoun_count = [0,0]
        self.pronoun_op = 0

        self.word_tracker = {}
        self.word_sorted = ['']

        # word length statistics
        self.word_lengths = []
        self.wl_avg = 0
        self.wl_min = 10
        self.wl_max = 0

        # sentence length, words/sentence stats
        self.sentences = 1
        self.sentence_toggle = False
        self.words_total = 0
        self.words_local = 0
        self.wps = [0]
        self.wps_avg = 0
        self.wps_min = 100
        self.wps_max = 0

        self.words = []

    def punctuation_check(self, word):
        ii = 0
        word = word.translate(None,':;-@#$%^&*()",')   # removes most non-alphanumeric characters
        if (not len(word)==0):      # prevent errors after symbol-only "words" get translated (e.g., "--")
            if (word[-1] == '.' or word[-1] =='!' or word[-1] =='?'):   #if end of sentence
                self.sentences += 1
                if len(word)==1:     #punctuation marks are seperate words
                    # reverse word count incremenents in listen()
                    self.words_local -= 1
                    self.words_total -= 1
                else:
                    word = word.translate(None,'!.?')   # removes punctuation
                self.sentence_toggle = True
        return word

    def word_length(self, word):      #calc various word length stats
        if (word == '.' or word =='!' or word =='?' or len(word)==0):    # if word is only punctuation or "empty" word
            pass            # do nothing
        else:
            self.word_lengths.append(len(word))
            self.wl_avg = sum(self.word_lengths)/float(len(self.word_lengths))
            if len(word) > self.wl_max:
                self.wl_max = len(word)
            if len(word) < self.wl_min:
                self.wl_min = len(word)
            # print self.wl_max, self.wl_min, self.wl_avg

    def pronoun_check(self, word):
        word = word.lower()     # forces lowercase
        if word in self.pronouns:
            # print word
            if word in self.pronoun_tracker:
                self.pronoun_tracker[word] += 1
            else:
                self.pronoun_tracker[word] = 1      #auto-creates entry for word
            inverse = [(value,key) for key,value in self.pronoun_tracker.items()]
            for ii in range(len(inverse)):
                if len(self.pronoun_sorted) < len(inverse):
                    self.pronoun_sorted.append('')
                    self.pronoun_count.append(0)
                self.pronoun_sorted[ii] = max(inverse)[1]
                self.pronoun_count[ii] = max(inverse)[0]
                inverse.remove(max(inverse))
            if word in self.pronouns_good:
                self.pronoun_op += 1
            elif word in self.pronouns_bad:
                self.pronoun_op -= 1

    def keyword_check(self, word, multiword = 'None'):          # similar structure as __pronoun_check()
        word = word.lower()
        if word in self.keywords:
            # print word
            if word in self.keyword_tracker:
                self.keyword_tracker[word] += 1
            else:
                self.keyword_tracker[word] = 1
            inverse = [(value,key) for key,value in self.keyword_tracker.items()]
            for ii in range(len(inverse)):
                if len(self.keyword_sorted) < len(inverse):
                    self.keyword_sorted.append('')
                    self.keyword_count.append(0)
                self.keyword_sorted[ii] = max(inverse)[1]
                inverse.remove(max(inverse))
            if word in self.keywords_good:
                self.keyword_op += 1
                print "Found keyword:", word
            return True     # for deciding on pronoun check
        elif multiword in self.keywords:
            if multiword in self.keyword_tracker:
                self.keyword_tracker[multiword] += 1
            else:
                self.keyword_tracker[multiword] = 1
            inverse = [(value,key) for key,value in self.keyword_tracker.items()]
            for ii in range(len(inverse)):
                if len(self.keyword_sorted) < len(inverse):
                    self.keyword_sorted.append('')
                    self.keyword_count.append(0)
                self.keyword_sorted[ii] = max(inverse)[1]
                inverse.remove(max(inverse))
            if multiword in self.keywords_good:
                self.keyword_op += 1
                print "Found keyword:", multiword
            return True
        else:
            return False

# This is an optional function for analyzing word frequency
    def __word_check(self, word):
        word = word.lower()
        if word in self.word_tracker:
            self.word_tracker[word] += 1
        else:
            self.word_tracker[word] = 1
        inverse = [(value,key) for key,value in self.word_tracker.items()]
        for ii in range(len(inverse)):
            if len(self.word_sorted) < len(inverse):
                self.word_sorted.append('')
            self.word_sorted[ii] = max(inverse)[1]
            inverse.remove(max(inverse))

    def words_per_sentence(self, word):
        # no adaptation needed if word is only punctuation
        # word count already dealt with in punctuation_check()
        if self.sentence_toggle == True:    # if end of sentence
            self.wps[-1] = self.words_local
            # wps max/min, reset
            if self.words_local > self.wps_max:
                self.wps_max = self.words_local
            if self.words_local < self.wps_min:
                self.wps_min = self.words_local
            self.wps.append(0)
            self.words_local = 0
            self.sentence_toggle = False
        self.wps_avg = sum(self.wps)/float(self.sentences)