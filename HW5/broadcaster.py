import zmq
import time
import random
import sys

__version__ = "0.3.0"

class SpeechBroadcast:
    def __init__(self, port,file_):

        # zmq communications
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        # binding to all ip addresses
        socket.bind("tcp://*:%s" % port)
        with open(file_,'r') as text:
            for line in text:
                for word in line.split():
                    topic = random.randrange(9999,10005)
                    messagedata = word
                    print "account sending: %s" % (messagedata)
                    socket.send("%s" % (messagedata))
                    time.sleep(0.025)

    # def setBalance(self, value):
    #     self.__balance = value

if __name__ == "__main__":
    if len(sys.argv) > 1:
        file_ = sys.argv[1]
    else:
        file_ = 'ihaveadream.txt'

    SpeechBroadcast("5556",file_)

# import re 
# sentence_end_finder=re.compile(r"[\.\?!]")
# if sentence_end_finder.findall(word): #or .match()?
