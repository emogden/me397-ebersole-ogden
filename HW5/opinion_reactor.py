import zmq
import sys
import time

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import pygame

__version__ = "0.5.0"

class OpinionReactor:
    def __init__(self,port_green, port_purple):
        self.__port_green = port_green
        self.__port_purple = port_purple
        self.t = 0
        self.int = 1
        self.green_op = 0
        self.purple_op = 0
        self.time_old = 0

        pygame.mixer.init()
        self.soundG = pygame.mixer.Sound("green.wav")
        self.soundP = pygame.mixer.Sound("purple.wav")
        self.switch = 0     #variable that enforces edge trigger

        context = zmq.Context()

        # creating socket to the green opinion
        self.socket_green = context.socket(zmq.SUB)
        self.socket_green.setsockopt(zmq.SUBSCRIBE, '')
        # connecting to the publisher
        self.socket_green.connect ("tcp://127.0.0.1:%s" % self.__port_green)

        # creating socket to the purple opinion
        self.socket_purple = context.socket(zmq.SUB)
        self.socket_purple.setsockopt(zmq.SUBSCRIBE, '')
        # connecting to the publisher
        self.socket_purple.connect ("tcp://127.0.0.1:%s" % self.__port_purple)

        # polling mechanism
        self.poller = zmq.Poller()
        self.poller.register(self.socket_green, zmq.POLLIN)
        self.poller.register(self.socket_purple, zmq.POLLIN)

        fig, self.ax = plt.subplots()
        self.line1, = self.ax.plot([], [], lw=2, color='g')
        self.line2, = self.ax.plot([], [], lw=2, color='purple')
        self.ax.set_ylim(0, 100)
        self.ax.set_xlim(0, 5)
        self.ax.grid()
        self.xdata, self.y1data, self.y2data = [], [], []

        self.time_start = time.time()
        #self.time_start = time.clock()
        ani = animation.FuncAnimation(fig, self.__run, blit=True, interval=self.int,repeat=False)
        plt.show()

    def __run(self,data):
        # polling with number indicting the wait in ms
        socks = dict(self.poller.poll())
        # print "waiting for opinions..."

        # print 'checking socket_green for msg'
        if self.socket_green in socks and socks[self.socket_green] == zmq.POLLIN:
            self.green_op = self.socket_green.recv()
            # print "green opinion", self.green_op
        # else: green_op = 0

        # print 'checking socket_purple for msg'
        if self.socket_purple in socks and socks[self.socket_purple] == zmq.POLLIN:
            self.purple_op = self.socket_purple.recv()
            # print "purple opinion", self.purple_op
        # else: purple_op = 0

#### Unix Implementation: comment out if on Windows

        if self.t == 0:
            self.time_old = time.time()
        # update the data
        self.t += (time.time() - self.time_old)
        self.time_old = time.time()

#### Also: be sure to change self.time_start assignment at end of __init__()
####
#### Windows Implementation: comment out if on Linux/Mac

        # if self.t == 0:
        #     self.time_old = time.clock()
        # # update the data
        # self.t += (time.clock() - self.time_old)
        # self.time_old = time.clock()

####

        # self.t += self.int/1000
        y1 = float(self.green_op)
        y2 = float(self.purple_op)
        self.xdata.append(self.t)
        self.y1data.append(y1)
        self.y2data.append(y2)
        xmin, xmax = self.ax.get_xlim()
        ymin, ymax = self.ax.get_ylim()

        if self.t >= xmax:
            self.ax.set_xlim(xmin, xmax + 5)
            self.ax.figure.canvas.draw()
        if (abs(y1) >= ymax or abs(y2) >= ymax):
            self.ax.set_ylim(2*ymin, 2*ymax)
            self.ax.figure.canvas.draw()

        self.line1.set_data(self.xdata, self.y1data)
        self.line2.set_data(self.xdata, self.y2data)

        # audio output (using ---) based on opinion comparison
        if self.green_op > self.purple_op and (self.switch == 1 or self.switch == 0):
            self.soundP.stop()
            self.soundG.play()
            self.switch = 2
        elif self.green_op < self.purple_op and (self.switch == 2 or self.switch == 0):
            self.soundG.stop()
            self.soundP.play()
            self.switch = 1

        print self.green_op, self.purple_op

        return self.line1, self.line2,

        

    def start(self):
        while True:
            # check for notifications
            # do something
            pass

if __name__ == "__main__":
  or_ = OpinionReactor(5557,5558)