import zmq
import time
import random
import sys
import re

__version__ = "0.2.0"

class SpeechBroadcast:
    def __init__(self, port,file_):

        # zmq communications
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        # binding to all ip addresses
        socket.bind("tcp://*:%s" % port)
        sentence = ""
        expression=re.compile(r"[\.\?!]")
        with open(file_,'r') as text:
            for line in text:
                for word in line.split(' '):
                    sentence=sentence+" "+word
                    if expression.findall(word):
                        print "account sending: %s" % (sentence)
                        socket.send("%s" % (sentence))
                        sentence = ""
                        time.sleep(0.1)

    # def setBalance(self, value):
    #     self.__balance = value

if __name__ == "__main__":
    if len(sys.argv) > 1:
        file_ = sys.argv[1]
    else:
        file_ = 'ihaveadream.txt'

    SpeechBroadcast("5556",file_)

# import re 
# sentence_end_finder=re.compile(r"[\.\?!]")
# if sentence_end_finder.findall(word): #or .match()?
