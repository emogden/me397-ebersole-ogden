from django.conf.urls import patterns, url

from main import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(?P<sample_id>\d+)/current.png$', views.current_sample, name='current_sample'),
    url(r'^(?P<download_id>\d+)/download$', views.download, name='download')
)