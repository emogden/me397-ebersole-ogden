# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20141214_1149'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sample',
            old_name='time',
            new_name='time_rec',
        ),
    ]
