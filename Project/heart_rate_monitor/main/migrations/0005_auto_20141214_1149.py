# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20141214_1145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sample',
            name='bpm',
            field=models.DecimalField(null=True, verbose_name=b'Average Beats per Minute', max_digits=5, decimal_places=2, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sample',
            name='duration',
            field=models.FloatField(null=True, verbose_name=b'Sample Duration', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sample',
            name='sdann',
            field=models.DecimalField(null=True, verbose_name=b'Heart Rate Variabilty (SDANN)', max_digits=4, decimal_places=4, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sample',
            name='time',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Time Recorded', null=True),
            preserve_default=True,
        ),
    ]
