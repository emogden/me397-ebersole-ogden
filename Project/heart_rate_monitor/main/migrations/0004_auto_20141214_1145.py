# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_sample_dataplot'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sample',
            name='dataplot',
        ),
        migrations.AddField(
            model_name='sample',
            name='bpm',
            field=models.DecimalField(default=Decimal('0'), max_digits=5, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sample',
            name='duration',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sample',
            name='sdann',
            field=models.DecimalField(default=Decimal('0'), max_digits=4, decimal_places=4),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sample',
            name='time',
            field=models.DateTimeField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
    ]
