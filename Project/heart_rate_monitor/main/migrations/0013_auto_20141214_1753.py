# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20141214_1752'),
    ]

    operations = [
        migrations.CreateModel(
            name='BPM',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.FloatField()),
                ('time', models.FloatField()),
                ('dataset', models.ForeignKey(to='main.Sample')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='beatsperminute',
            name='dataset',
        ),
        migrations.DeleteModel(
            name='Beatsperminute',
        ),
    ]
