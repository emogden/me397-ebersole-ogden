# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0008_auto_20141214_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sample',
            name='bpm',
            field=models.FloatField(null=True, verbose_name=b'Average Beats per Minute', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sample',
            name='sdann',
            field=models.FloatField(null=True, verbose_name=b'Heart Rate Variabilty (SDANN)', blank=True),
            preserve_default=True,
        ),
    ]
