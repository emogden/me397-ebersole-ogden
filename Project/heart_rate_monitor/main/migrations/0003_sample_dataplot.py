# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20141204_1225'),
    ]

    operations = [
        migrations.AddField(
            model_name='sample',
            name='dataplot',
            field=models.ImageField(default=b'main/default_plot.png', upload_to=b''),
            preserve_default=True,
        ),
    ]
