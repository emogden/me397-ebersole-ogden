# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20141214_1726'),
    ]

    operations = [
        migrations.CreateModel(
            name='BPM',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.FloatField()),
                ('time', models.FloatField()),
                ('dataset', models.ForeignKey(to='main.Sample')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameField(
            model_name='sample',
            old_name='bpm',
            new_name='bpm_avg',
        ),
    ]
