# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sample',
            fields=[
                ('id', models.IntegerField(unique=True, serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='entry',
            name='dataset',
            field=models.ForeignKey(to='main.Sample'),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='Dataset',
        ),
    ]
