from django.db import models
from django.utils import timezone
import datetime

class Sample(models.Model):
	id = models.IntegerField(unique=True,primary_key=True)
	bpm_avg = models.FloatField(verbose_name="Average Beats per Minute",
		blank=True,null=True)
	sdann = models.FloatField(verbose_name="Heart Rate Variabilty (SDANN)",
		blank=True,null=True)
	duration = models.FloatField(verbose_name="Sample Duration",
		blank=True,null=True)
	time_rec = models.DateTimeField(verbose_name="Time Recorded",
		auto_now_add=True,blank=True,null=True) 	#sets time automatically on creation

	def get_menustring(self):		# for use in menus
		return 'Sample {0}: {1}'.format(self.id, 
			(timezone.localtime(self.time_rec)).strftime('%Y-%m-%d %H:%M'))

	def __unicode__(self):
		return str(self.id)

class Voltage(models.Model):
	dataset = models.ForeignKey(Sample)
	value = models.FloatField()
	time = models.FloatField()

	def __unicode__(self):
		return str((self.time,self.value))

class BPM(models.Model):
	dataset = models.ForeignKey(Sample)
	value = models.FloatField()
	time = models.FloatField()

	def __unicode__(self):
		return str((self.time,self.value))