from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

from main.models import Sample, Voltage

def index(request):
	if 'sample_dropdown' in request.POST:
		key = request.POST['sample_dropdown']
	else:
		key = Sample.objects.last().id
	list_ = Sample.objects.all()
	current_ = Sample.objects.get(pk=key)

	return render(request, 'main/main.html', {'sample_list': list_, 'sample_id': key, 'sample': current_})
	# return HttpResponse(A)

def current_sample(request, sample_id = 0):
	fig=Figure(facecolor = "white")

	if sample_id:
		Data = Sample.objects.get(pk=sample_id)
	else:
		Data = Sample.objects.last()

	if Data.bpm_set.all():
		ax1 = fig.add_subplot(211)
		ax2 = fig.add_subplot(212)
	else:
		ax1 = fig.add_subplot(111)
		ax1.set_xlabel("Time (seconds)")

	xmax = Data.voltage_set.values_list().last()[3]
	ax1.axis([0,xmax,0,256])
	ax1.set_axis_bgcolor('white')
	ax1.set_title("Heart Rate Measurements -- Sample %i" % int(sample_id))
	ax1.set_ylabel("Relative Sensor Voltage")
	xx=[]
	yy=[]
	line1, = ax1.plot([], [], lw=1)
	ii = 0
	for item in Data.voltage_set.all():
		ii += 1
		xx.append(item.time)
		yy.append(item.value)
	line1.set_data(xx,yy)

	if Data.bpm_set.all():
		xmax = Data.bpm_set.values_list().last()[3]
		ax2.axis([0,xmax,0,180])
		line2, = ax2.plot([], [], lw=1)
		xx=[]
		yy=[]
		ii = 0
		for item in Data.bpm_set.all():
			ii += 1
			xx.append(item.time)
			yy.append(item.value)
		line2.set_data(xx,yy)
		ax2.set_ylabel("Beats per Minute (bpm)")
		ax2.set_xlabel("Time (seconds)")

	canvas=FigureCanvas(fig)
	response=HttpResponse(content_type='image/png')
	canvas.print_png(response)
	return response

def download(request, download_id):
    # response = HttpResponse(content_type='text/csv')
    # response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

    # writer = csv.writer(response)
    # writer.writerow(['First row', 'Foo', 'Bar', 'Baz'])
    # writer.writerow(['Second row', 'A', 'B', 'C', '"Testing"', "Here's a quote"])

    # return response

	p = get_object_or_404(Sample, pk=download_id)
	response = HttpResponse(content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename="Sample%i.txt"' % int(download_id)
	for item in p.voltage_set.all():
		str_temp = ""
		str_temp += str(item.time)
		str_temp += " "
		str_temp += str(item.value)
		str_temp += "\n"
		response.write(str_temp)
	return response

