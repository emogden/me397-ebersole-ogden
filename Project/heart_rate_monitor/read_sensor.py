import zmq
import smbus
import time
import commands

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #Button Input
GPIO.setup(18, GPIO.OUT) #Green LED
GPIO.setup(22, GPIO.OUT) #Red LED

def led_flash(): #Green
	GPIO.output(18, True)
	time.sleep(0.1)
	GPIO.output(18, False)

def led2_flash(): #Red
	GPIO.output(22, True)
	time.sleep(0.1)
	GPIO.output(22, False)

def countdown():
	# print "Reading will begin in 5,"
	led_flash()
	time.sleep(0.9)
	# print "4,"
	led_flash()
	time.sleep(0.9)
	# print "3,"
	led_flash()
	time.sleep(0.9)
	# print "2,"
	led_flash()
	time.sleep(0.9)
	# print "1,"
	led_flash()
	time.sleep(0.9)

def display_ip(): 
	# Fetch local ip
	ip = commands.getoutput("ip addr show wlan0 | grep inet").split()[1]
	print "Displaying IP"
	time.sleep(1)
	for num in ip:
		if num == "0":
			led_flash()
			time.sleep(0.1)
			led_flash()
			time.sleep(1)
		elif num == "/":
			break
		elif num == ".":
			led2_flash()
			time.sleep(1)
		else:
			for ii in range(int(num)):
				led_flash()
				time.sleep(0.5)
			time.sleep(1)
	return ip

# def rs_data():		#read and send

if __name__=="__main__":
	context = zmq.Context()
	socket = context.socket(zmq.PUB)
	socket.bind("tcp://*:5556")

	channel = 0x00
	address = 0x48
	input = 0x02
	pulse = False
	bus = smbus.SMBus(1)
	ip = "10.146.196.12"
	dt = 0.01

	# print "Communicate IP?"
	GPIO.output(18, True)
	tt = 0.0
	ip_display = False
	# Pause for 5 seconds, asking user if IP should be displayed
	while tt < 5:
		if GPIO.input(23) == True:
			ip_display = True
			break
		tt += 0.1
		time.sleep(0.1)
	GPIO.output(18, False)

	# Display IP if requested
	if ip_display == True:
		ip=display_ip()

	while True:
		try:
			# print "Waiting for command"
			GPIO.wait_for_edge(23, GPIO.RISING) #Blocking call on button
			countdown()
			GPIO.output(18, True)
			tt = 0.0
			while ( GPIO.input(23) == False ): #As long as button is not depressed
				value = bus.read_i2c_block_data(address,input)[0]
				# Flash LED on beat
				if value > 140:
					if pulse == False: #prevents multiple flashes if value sustained above 140
						pulse = True
						# print "Beat"
						led2_flash()
					else:
						pass
						# print "."
				else:
					pulse = False
					# print "."
				tt += dt
				socket.send("%f %d" % (tt, value))
				time.sleep(dt)		# assuming previous computations take negligible time
			# print "Reading complete"
			GPIO.output(18,False)
			time.sleep(1)

		except KeyboardInterrupt:	
			GPIO.cleanup()
			break
