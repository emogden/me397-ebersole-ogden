import os
os.environ["DJANGO_SETTINGS_MODULE"] = "heart_rate_monitor.settings"
# export DJANGO_SETTINGS_MODULE=mysite.settings
import django
django.setup()
from button.models import Number

ii = 1
A = Number.objects.get(id=1)
while True:
	A.value = ii
	if ii%100 == 0:
		A.save()
	ii += 1