import os
os.environ["DJANGO_SETTINGS_MODULE"] = "heart_rate_monitor.settings"
# export DJANGO_SETTINGS_MODULE=mysite.settings
import django
django.setup()
from main.models import Sample,Voltage
from django.db import transaction
import random

#This code generates a database entry (just voltage data) from a text file

#Pick a random text file from the 3 available
dataset = random.randint(1,3)

#Unless the database is empty, set the id of the new set = 1 + the previous
if Sample.objects.all():
	new_id = Sample.objects.last().id + 1
else:
	new_id = 1

new_sample = Sample(id = new_id)

ii = 0
with transaction.atomic():				#Essential to timely operation
	with open('heartbeat_%i.txt' % dataset,'r') as data:
	    for voltage in data:
	        data_point = voltage.split()
	        new_sample.voltage_set.create(
	        	time = float(data_point[0]),
	        	value = float(data_point[1])
	        	)

new_sample.save()
