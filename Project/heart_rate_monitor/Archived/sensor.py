import os
from time import sleep
os.environ["DJANGO_SETTINGS_MODULE"] = "heart_rate_monitor.settings"
# export DJANGO_SETTINGS_MODULE=mysite.settings
import django
django.setup()
from button.models import Number

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN)

ii = 0
A = Number.objects.get(id=1)
A.value = ii
A.save()
while True:
	if ( GPIO.input(23) == True ):
		ii += 1
		print ii
		A.value = ii
		A.save()
		sleep(0.25)