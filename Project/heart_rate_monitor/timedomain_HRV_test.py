# Following code based on Arduino code provided by sensor developers
# http://pulsesensor.myshopify.com/pages/code-and-guide

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as ssig

# calc_stats() used by reactor.py; "running as main" to be used with "write_latest_to_text.py"

def calc_stats(timedata,voltdata):
	# voltdata2=voltdata-np.mean(voltdata)	#removes DC bias from signal
	# voltdata3=ssig.savgol_filter(voltdata2,61,2)	#applies minor filter to data

	lastBeat=0 		#time of last beat
	thresh=140		#beat threshold
	IBI=0 		#inter beat interval
	firstBeat = True	#first, second beats to allow for special statistics handling
	secondBeat = False
	pulse = False
	maxsig=0
	minsig=255
	BPM=-1 		# beats per minute - average over entire sample
	HRI=[]
	HR_size=7		# for BPM calcs with few beats
	HR=[0,0,0,0,0,0,0]	# size matches HR_size

	for ii in range(0,len(voltdata)):
		curtime=timedata[ii]
		signal=voltdata[ii]
		N=curtime-lastBeat		# time since last beat

		# set max, mins 
		if(signal<minsig):
			minsig=signal
		if(signal>maxsig):
			maxsig=signal

		# recognized pulses cannot occur within .3 seconds (max HR of 200 BPM)
		# http://www.heart.org/HEARTORG/GettingHealthy/PhysicalActivity/FitnessBasics/Target-Heart-Rates_UCM_434341_Article.jsp#
		if N>(.3):
			if (signal>thresh and pulse==False and N>IBI*.6):	# IBI*.6 - convention for max variability in sequential beats
				Pulse=True
				IBI=N
				HRI.append(IBI)		# add to total log of IBI's
				lastBeat=curtime

				if secondBeat:
					secondBeat=False
					for i in range(0,HR_size):	#populate all of HR for approximate statistics
						HR[i]=IBI

				if firstBeat:
					firstBeat=False
					secondBeat=True

				HRsum=0
				if len(HRI)<=len(HR):		# if few beats
					for i in range(0,HR_size-1):
						HR[i]=HR[i+1]
					HR[len(HR)-1]=IBI
					HRsum=np.sum(HR)
					HRavg=HRsum/len(HR)
					BPM=60/HRavg

				else:						# assuming alternative is susbstantial number of beats
					BPM=60/np.mean(HRI)

		if (signal<thresh and pulse == True):	# end of pulse
			pulse=False
			# thresh=(maxsig+minsig)/2 	# reset threshold

	#debug
	# print HRI
	print "BPM:",BPM
	print "Average NN interval:",np.mean(HRI)
	print "HRV (SDANN):",np.std(HRI)

	return BPM, np.std(HRI)

	# SEE http://content.onlinejacc.org/article.aspx?articleid=1124431#Results for possible healthy values (.135 for SDANN?)

if __name__=="__main__":
	data=np.loadtxt("latest_heartbeat.txt")
	timedata=data[:,0]
	voltdata=data[:,1]
	calc_stats(timedata,voltdata)