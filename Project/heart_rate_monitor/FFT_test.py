# http://stackoverflow.com/questions/20634833/scipy-numpy-fft-on-data-from-file

# This file is left in as a debug option to compare with timedomain_HRV_test.py

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as ssig

datatest=np.loadtxt("latest_heartbeat.txt")		# read from text file (use in conjunction with "write_latest_to_text.py")
timedata=datatest[:,0]
voltdata=datatest[:,1]
voltdata2=voltdata-np.mean(voltdata)	#removes DC bias from signal
voltdata3=ssig.savgol_filter(voltdata2,71,2)		# applies filter to data

plt.figure(1)
plt.subplot(211)
plt.plot(timedata,voltdata)
plt.title("Input data")

FFTdata=np.fft.rfft(voltdata3)			# "rfft" - removes negative frequency components
FFTfreq=np.fft.rfftfreq(len(voltdata3),d=.02)	# d is timestep used in data

plt.subplot(212)
plt.plot(FFTfreq,FFTdata)
plt.axis([0,5,-300,1500])
plt.title("FFT of data")
plt.show()