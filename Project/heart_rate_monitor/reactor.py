import zmq
import time
import sys
import select

#for statistics calculating
import write_latest_to_text
import timedomain_HRV_test

import os
os.environ["DJANGO_SETTINGS_MODULE"] = "heart_rate_monitor.settings"
# export DJANGO_SETTINGS_MODULE=mysite.settings
import django
django.setup()
from main.models import Sample,Voltage
from django.db import transaction

def get_id():
	if Sample.objects.all():
		new_id = Sample.objects.last().id + 1
	else:
		new_id = 1
	return new_id

if __name__ == "__main__":
	ip = "127.0.0.1"
	port = "5556"
	key = ""

	# Allows the ip and port to be passed in as command line args
	if len(sys.argv) > 1:
		ip =  str(sys.argv[1])
		if len(sys.argv) > 2:
			port_op = str(sys.argv[2])
			if len(sys.argv) > 3:
				print "Too many arguments!! Usage: python reactor.py [ip_addr] [port]"
				exit()

	context = zmq.Context()

	# creating socket to the account
	socket_values = context.socket(zmq.SUB)
	socket_values.setsockopt(zmq.SUBSCRIBE, '')
	socket_values.setsockopt(zmq.RCVTIMEO, 1000) #ESSENTIAL
	# connecting to the publisher
	socket_values.connect ("tcp://"+ip + ":" + port)


	poller = zmq.Poller()
	poller.register(socket_values, zmq.POLLIN)
	print "listening..."

	ii = 0
	tt = 0
	dt = 0.01
	save = False

	while True:
		try:
			# # polling with infinite wait initially, will never block after first pass (since read_sensor doesn't close the socket once opened)
			socks = dict(poller.poll())
			if socket_values in socks and socks[socket_values] == zmq.POLLIN:
				id_ = get_id()
				save = True
				new_sample = Sample(id = id_)
				print "Reading new sample!"
				while socket_values in socks and socks[socket_values] == zmq.POLLIN: #If this while loop exits, no more data is coming through
					(time_,value_) = (socket_values.recv().split())   #recv has a timeout of 1 second, as set above in line 44. After that time, recv will no longer block.
					new_sample.voltage_set.create(time = float(time_), value = float(value_))

		except KeyboardInterrupt:
			break
		except zmq.error.Again as e:
				if "Resource temporarily unavailable" in str(e):
					pass
				else:
					# Oopsy other error re-raise
					raise
		if save == False:
			print "Save cancelled."

		if save == True:
			print "Saving sample!"
			new_sample.save()		# first save fills Voltage data needed for statistics
			# get bpm, sdann, duration
			(timedata, voltdata)=write_latest_to_text.generate_lists(id_)
			new_sample.duration=timedata[-1]-timedata[0]	# since t_0 often =/= 0
			(new_sample.bpm_avg, new_sample.sdann)=timedomain_HRV_test.calc_stats(timedata,voltdata)
			new_sample.save()
			save = False
