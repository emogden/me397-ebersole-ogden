import numpy as np 
import random
import matplotlib.pyplot as plt
from django.core.files import File

# Debug file - plots data from text file

count = 0
with open('latest_heartbeat.txt') as data:
	for line in data:
		# print line.split()
		count += 1
	heartbeat = np.zeros((count,2))

count = 0
with open('latest_heartbeat.txt') as data:
	for line in data:
		heartbeat[count] = line.split()
		# print heartbeat[count]
		count += 1

print "Using Latest Dataset"

fig = plt.figure()
xmax = heartbeat[:,0].max()
ax = plt.axes(xlim=(0,xmax),ylim=(50,200))
line, = ax.plot([], [], lw=1)
xx = []
yy = []
for item in heartbeat:
	xx.append(item[0])
	yy.append(item[1])
line.set_data(xx,yy)
fig.show()
while True:
	pass
