import numpy as np 
import random
import matplotlib.pyplot as plt
from django.core.files import File

import os
os.environ["DJANGO_SETTINGS_MODULE"] = "heart_rate_monitor.settings"
# export DJANGO_SETTINGS_MODULE=mysite.settings
import django
django.setup()
from main.models import Sample, Voltage

# If run as main - debug file; generate_lists() used in reactor.py

def generate_lists(sample_id):		# generate, return temporary lists by reading sample's entries
	latest_dataset=Voltage.objects.filter(dataset=sample_id)
	timeset=[]
	voltageset=[]

	for datapt in latest_dataset:
		timeset.append(datapt.time)
		voltageset.append(datapt.value)

	return (timeset, voltageset)


if __name__=="__main__":
	file1=open('latest_heartbeat.txt','w')
	(timeset, voltageset)=generate_lists(8)
	if len(timeset)==len(voltageset):	# don't write to file unless proper data set
		for ii in range(0,len(timeset)):
			str_temp=str(timeset[ii])
			str_temp+="\t"
			str_temp+=str(voltageset[ii])
			str_temp+="\n"
			file1.write(str_temp)